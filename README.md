CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Developing a plugin
 * Installation
 * Configuration
 * Developers
 * Maintainers

INTRODUCTION
------------
The overview builder is a developer module that adds a new plugin type
"overview_builder" that allows to add an overview (views or custom) to your
entities. The benefit of this module is that your overview pages are also
e.g. nodes, so the path, metatags, header and footer text, ... can be
configured by a content manager.

This module depends on the "pluginreference" module to reference to your
overview plugin. When the "paragraphs" module is enabled, this module
will create a default "overview" paragraph with an overview field from where
you can reference to your custom plugins. Your overviews can now be used
in a paragraph setup.

When you do not prefer paragraphs, you can create your own "overview" field by
adding a field that references to the OverviewBuilder plugin.

REQUIREMENTS
------------
- pluginreference
- paragraphs (optional)

INSTALLATION
------------
Install the "Overview Builder" module as
you would normally install a contributed Drupal module.

Visit https://www.drupal.org/node/1897420 for further
information.

When the "paragraphs" module is enabled, this module will create a default
"overview" paragraph with an overview field from where you can reference
to your custom plugins.

DEVELOPING A PLUGIN
-------------------
There is an overview_builder_example module, located in
modules/overview_builder_example

To create a plugin. Create a file in
``src/Plugin/OverviewBuilder``

This module provides 2 base plugin classes:

### Views overview

When you want to reference to a views overview, the following annotation is
required:
```
@OverviewBuilder(
  id = "views_overview",
  label = @Translation("Views Overview"),
  view_id = "news",
  display_id = "page_1",
)
```

You can extend the `ViewsOverviewBuilderBase` class which contains
predefined logic to load and show the view.

And that's it. After clearing the cache, and referencing to your plugin, the
view overview will be displayed on your entity.

### Custom overview

When the views module is not sufficient for your needs, or you need more
flexibility, you can create a custom overview. the following annotation
can be used:
```
@OverviewBuilder(
  id = "advanced_node_overview",
  label = @Translation("Advanced Node overview"),
  entity_type = "node",
  bundle = "news",
  view_mode = "teaser",
  ajax = true,
)
````

You can extend the `CustomOverviewBuilderBase` class which contains
predefined logic to build the overview.

The `id` property is a unique identifier for the plugin. This option is
required.

The `label` property is the label that will be shown in the overview
field. This option is required.

The `entity_type` property defines the entity type that is used in the overview.
This entity_type is used for setting cache_tags for your overview.
The view_mode property defines the view_mode of your entities that is used
in the overview. This option is required.

The `bundle` property can be added to define which bundle type of the
given entity is shown. This property will be used to add an
ENTITY_TYPE_list:BUNDLE cache tag. When the bundle is not configured,
the ENTITY_TYPE_list cache tag will be used as fallback. This option
is not required.

The `view_mode` property is used to define which view_mode you want to
show in the overview. This option is not required, but the default
fallback is the teaser view mode.

The `ajax` property will enable ajax refreshing of the overview when
enabled. This option is not required and the default fallback is false.

The `CustomOverviewBuilderBase` class contains the following methods
to customize the overview.

The `getListEntities()` method in your custom plugin returns the entities
that are shown in the overview. By default, all entities will be shown.

the `buildFilters` method builds the filters. It's possible to return a
form that extends the `CustomOverviewBuilderFiltersFormBase` class or
a renderable array.

the `getEmptyText` method returns the default text that is shown when
there are no results.

the `buildItems` method builds the items that are shown in the overview.
You can override this method to have e.g. multiple view modes in your overview.

the `buildPager` method builds the pager that is shown at the bottom of the
overview.

the `build` method is the method that binds everything together.

CONFIGURATION
-------------
When the "paragraphs" module is enabled, this module will create a default
"overview" paragraph with an overview field from where you can reference
to your custom plugins.

When creating a node you can now add an "overview" paragraph. In this
paragraph you can reference your custom overview.

When your project does not contain a paragraph setup, you can add your own
"overview" field.

On an entity of your choice e.g. Node, add a new field of the type
"Plugin reference" and reference to the
"Drupal\overview_builder\OverviewBuilderManager" plugin. On the manage display
page, choose the format "Plugin Reference overview builder" to show your
overview.

DEVELOPERS
----------
* Jeroen Tubex - https://www.drupal.org/u/jeroent
* Arno Van de Weyer - https://www.drupal.org/u/waldoswndrwrld

MAINTAINERS
-----------
Current maintainers:
* Jeroen Tubex - https://www.drupal.org/u/jeroent
* Arno Van de Weyer - https://www.drupal.org/u/waldoswndrwrld
