(function (Drupal, drupalSettings) {
  'use strict';

  Drupal.overview_builder_ajax_overview = Drupal.overview_builder_ajax_overview || {};

  Drupal.behaviors.overview_builder_ajax_overview = {
    attach: function (context, settings) {
      context.querySelectorAll('.js-overview-builder-overview').forEach(function (overview) {
        if (overview.dataset.triggered) {
          return;
        }
        overview.dataset.triggered = true;
        let filters = overview.querySelector('.js-overview-builder-filters');
        if (filters) {
          filters.addEventListener('submit', function (event) {
            event.preventDefault();
            let form_data = new FormData(this);
            Drupal.overview_builder_ajax_overview.updateOverview(this.closest('.js-overview-builder-overview'), form_data);
          });
        }
        overview.querySelectorAll('.js-pager__items a').forEach(function (pager_link) {
          pager_link.addEventListener('click', function (event) {
            event.preventDefault();
            let form_data = new FormData();
            let url_search_params = Drupal.overview_builder_ajax_overview.getCurrentQueryParams();
            let pager_link_params = new URLSearchParams(this.getAttribute('href'));
            url_search_params.set('page', pager_link_params.get('page'));
            url_search_params.forEach(function (value, key) {
              form_data.append(key, value);
            });
            Drupal.overview_builder_ajax_overview.updateOverview(this.closest('.js-overview-builder-overview'), form_data);
          });
        });
      });
    }
  };

  Drupal.overview_builder_ajax_overview.updateOverview = function (overview, form_data) {
    let ajax = Drupal.ajax({
      url: Drupal.overview_builder_ajax_overview.ajaxUrl(overview, form_data),
      type: "POST",
      submit: {
        'path': drupalSettings.path.currentPath,
      },
      progress: {
        type: 'fullscreen'
      }
    });

    ajax.execute().done(function() {
      overview.scrollIntoView();
      Drupal.attachBehaviors();
      Drupal.overview_builder_ajax_overview.updateUrl(form_data);
    });
  };

  Drupal.overview_builder_ajax_overview.ajaxUrl = function (overview, form_data) {
    let url = 'ajax/overview-builder/' + overview.dataset.overviewId;
    let queryString = Drupal.overview_builder_ajax_overview.buildQueryString(form_data);
    if (queryString) {
      url += '?' + queryString;
    }
    return Drupal.url(url);
  };

  Drupal.overview_builder_ajax_overview.buildGetParams = function (form_data) {
    let entries = form_data.entries();
    return [...entries].filter(Boolean).filter(function (x) {
      return x[1] && x[1] != null && x[1].length !== 0;
    });
  }

  Drupal.overview_builder_ajax_overview.updateUrl = function (form_data) {
    let url = window.location.href;
    if (url.indexOf("?") !== -1) {
      url = url.split("?")[0];
    }

    if (typeof window.history.pushState == 'function') {
      let queryString = Drupal.overview_builder_ajax_overview.buildQueryString(form_data);
      if (queryString) {
        url += '?' + queryString;
      }
      window.history.pushState({}, "Hide", url);
    }
  }

  Drupal.overview_builder_ajax_overview.buildQueryString = function (form_data) {
    let params = Drupal.overview_builder_ajax_overview.buildGetParams(form_data);
    return params.map(function (x) {
      if (x[1]) {
        return encodeURIComponent(x[0]) + '=' + encodeURIComponent(x[1]);
      }
      return null;
    }).join('&');
  }

  Drupal.overview_builder_ajax_overview.getCurrentQueryParams = function () {
    let url = window.location.href;
    let query_string = '';
    if (url.indexOf("?") !== -1) {
      query_string = url.split("?")[1];
    }
    return new URLSearchParams(query_string);
  }

})(Drupal, drupalSettings);
