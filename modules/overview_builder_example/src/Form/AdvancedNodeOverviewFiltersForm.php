<?php

namespace Drupal\overview_builder_example\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\overview_builder\Form\CustomOverviewBuilderFiltersFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Example filters form for the advanced node overview.
 *
 * @package Drupal\overview_builder_example\Form
 */
class AdvancedNodeOverviewFiltersForm extends CustomOverviewBuilderFiltersFormBase {

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $taxonomyTermStorage;

  /**
   * AdvancedNodeOverviewFiltersForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->taxonomyTermStorage = $entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'advanced_node_overview_filters_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $this->addMultiOptionsFormElement($form, 'tag', $this->t('Tags'), $this->getNewsTagOptions());
    $this->addMultiOptionsFormElement($form, 'category', $this->t('Category'), $this->getNewsCategoryOptions());
    return parent::buildForm($form, $form_state);
  }

  /**
   * Build the news tag options.
   *
   * @return array
   *   An array containing the news tag option as key and label as value.
   */
  protected function getNewsTagOptions(): array {
    $options = [];
    /** @var \Drupal\taxonomy\TermInterface[] $news_tags */
    $news_tags = $this->taxonomyTermStorage->loadByProperties(['vid' => 'news_tag']);

    foreach ($news_tags as $news_tag) {
      $options[$news_tag->id()] = $news_tag->label();
    }

    return $options;
  }

  /**
   * Build the news tag options.
   *
   * @return array
   *   An array containing the news tag option as key and label as value.
   */
  protected function getNewsCategoryOptions(): array {
    $options = [];
    /** @var \Drupal\taxonomy\TermInterface[] $news_tags */
    $news_tags = $this->taxonomyTermStorage->loadByProperties(['vid' => 'news_category']);

    foreach ($news_tags as $news_tag) {
      $options[$news_tag->id()] = $news_tag->label();
    }

    return $options;
  }

}
