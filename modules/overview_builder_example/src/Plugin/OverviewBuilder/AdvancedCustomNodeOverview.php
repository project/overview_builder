<?php

namespace Drupal\overview_builder_example\Plugin\OverviewBuilder;

use Drupal\node\NodeInterface;
use Drupal\overview_builder\CustomOverviewBuilderBase;
use Drupal\overview_builder_example\Form\AdvancedNodeOverviewFiltersForm;

/**
 * Advanced custom node overview builder.
 *
 * @OverviewBuilder(
 *   id = "advanced_node_overview",
 *   label = @Translation("Advanced Node overview"),
 *   entity_type = "node",
 *   view_mode = "teaser",
 *   bundle = "news",
 *   ajax = true,
 * )
 */
class AdvancedCustomNodeOverview extends CustomOverviewBuilderBase {

  /**
   * Nr of items shown per page.
   */
  protected const ITEMS_PER_PAGE = 20;

  /**
   * {@inheritdoc}
   */
  public function getListEntities(): array {
    return $this->getNewsItems(
      $this->currentRequest->get('tag'),
      $this->currentRequest->get('category'),
      self::ITEMS_PER_PAGE
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function buildFilters(): ?array {
    return $this->formBuilder->getForm(AdvancedNodeOverviewFiltersForm::class);
  }

  /**
   * Get all news items by possible filters.
   *
   * @param int|null $tag
   *   The news tag.
   * @param int|null $category
   *   The news category.
   * @param int|null $limit
   *   Nr of items to show per page.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array containing news items.
   */
  protected function getNewsItems(int $tag = NULL, int $category = NULL, int $limit = NULL) {
    $query = $this->entityStorage->getQuery();

    $query->condition('status', NodeInterface::PUBLISHED);
    $query->condition('type', 'news');

    if (!empty($tag)) {
      $query->condition('field_tag', $tag);
    }

    if (!empty($category)) {
      $query->condition('field_news_category', $category);
    }

    if (!empty($limit)) {
      $query->pager($limit);
    }

    $results = (array) $query->execute();

    if (empty($results)) {
      return [];
    }

    return $this->entityStorage->loadMultiple($results);
  }

}
