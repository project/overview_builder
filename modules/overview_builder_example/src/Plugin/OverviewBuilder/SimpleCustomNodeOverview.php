<?php

namespace Drupal\overview_builder_example\Plugin\OverviewBuilder;

use Drupal\overview_builder\CustomOverviewBuilderBase;

/**
 * Simple custom node overview builder.
 *
 * @OverviewBuilder(
 *   id = "simple_node_overview",
 *   label = @Translation("Simple Node overview"),
 *   entity_type = "node",
 *   view_mode = "teaser",
 * )
 */
class SimpleCustomNodeOverview extends CustomOverviewBuilderBase {

  /**
   * Nr of items shown per page.
   */
  protected const ITEMS_PER_PAGE = NULL;

  /**
   * {@inheritdoc}
   */
  public function getListEntities(): array {
    return $this->entityStorage->loadByProperties(['type' => 'page']);
  }

}
