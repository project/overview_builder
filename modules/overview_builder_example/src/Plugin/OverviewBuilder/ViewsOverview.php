<?php

namespace Drupal\overview_builder_example\Plugin\OverviewBuilder;

use Drupal\overview_builder\ViewsOverviewBuilderBase;

/**
 * News overview builder.
 *
 * @OverviewBuilder(
 *   id = "views_overview",
 *   label = @Translation("Views overview"),
 *   view_id = "news",
 *   display_id = "page_1",
 * )
 */
class ViewsOverview extends ViewsOverviewBuilderBase {

}
