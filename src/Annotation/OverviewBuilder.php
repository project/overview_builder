<?php

namespace Drupal\overview_builder\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an overview builder annotation object.
 *
 * Plugin Namespace: Plugin\OverviewBuilder.
 *
 * @see \Drupal\overview_builder\OverviewBuilderManager
 * @see plugin_api
 *
 * @Annotation
 */
class OverviewBuilder extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Human-readable of the overview builder.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * The entity_type used in the overview builder.
   *
   * This setting is used when building a custom overview.
   *
   * @var string
   *
   * @see \Drupal\overview_builder\CustomOverviewBuilderBase
   */
  public $entity_type;

  /**
   * The bundle used in the overview builder.
   *
   * This setting is used when building a custom overview.
   *
   * @var string
   *
   * @see \Drupal\overview_builder\CustomOverviewBuilderBase
   */
  public $bundle;

  /**
   * The view_mode used in the overview builder.
   *
   * This setting is used when building a custom overview.
   *
   * @var string
   *
   * @see \Drupal\overview_builder\CustomOverviewBuilderBase
   */
  public $view_mode;

  /**
   * Ajax is enabled for this overview.
   *
   * This setting is used when building a custom overview.
   *
   * @var string
   *
   * @see \Drupal\overview_builder\CustomOverviewBuilderBase
   */
  public $ajax;

  /**
   * The view_id used in the overview builder.
   *
   * This setting is used when building a views overview.
   *
   * @var string
   *
   * @see \Drupal\overview_builder\ViewsOverviewBuilderBase
   */
  public $view_id;

  /**
   * The display_id used in the overview builder.
   *
   * This setting is used when building a views overview.
   *
   * @var string
   *
   * @see \Drupal\overview_builder\ViewsOverviewBuilderBase
   */
  public $display_id;

}
