<?php

namespace Drupal\overview_builder\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\overview_builder\OverviewBuilderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;

/**
 * Controller used to rebuild overview that have ajax enabled.
 *
 * @package Drupal\overview_builder\Controller
 */
class AjaxController extends ControllerBase {

  /**
   * The overview builder manager.
   *
   * @var \Drupal\overview_builder\OverviewBuilderManager
   */
  protected $overviewBuilderManager;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The dynamic router service.
   *
   * @var \Symfony\Component\Routing\Matcher\RequestMatcherInterface
   */
  protected $router;

  /**
   * AjaxController constructor.
   *
   * @param \Drupal\overview_builder\OverviewBuilderManager $overviewBuilderManager
   *   The overview builder manager.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Symfony\Component\Routing\Matcher\RequestMatcherInterface $router
   *   The dynamic router service.
   */
  public function __construct(OverviewBuilderManager $overviewBuilderManager, PathValidatorInterface $path_validator, RendererInterface $renderer, RequestStack $request_stack, RequestMatcherInterface $router) {
    $this->overviewBuilderManager = $overviewBuilderManager;
    $this->pathValidator = $path_validator;
    $this->renderer = $renderer;
    $this->requestStack = $request_stack;
    $this->router = $router;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.overview_builder'),
      $container->get('path.validator'),
      $container->get('renderer'),
      $container->get('request_stack'),
      $container->get('router')
    );
  }

  /**
   * Callback for /ajax/overview-builder/{plugin_id}.
   *
   * Builds the overview that can be used as ajax callback.
   *
   * @param string $plugin_id
   *   The plugin ID of the overview that should be build.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response representing the overview.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function overview(string $plugin_id, Request $request): AjaxResponse {
    if (!$this->overviewBuilderManager->hasDefinition($plugin_id)) {
      throw new NotFoundHttpException();
    }

    $plugin_definition = $this->overviewBuilderManager->getDefinition($plugin_id);

    $path = $this->getPathFromRequest($request);
    if (empty($plugin_definition['ajax']) || $plugin_definition['ajax'] === FALSE || $path === NULL || !$this->pathValidator->isValid($path)) {
      throw new NotFoundHttpException();
    }

    $this->overrideRequest($request);
    /** @var \Drupal\overview_builder\OverviewBuilderInterface $plugin */
    $plugin = $this->overviewBuilderManager->createInstance($plugin_id);
    $build = $plugin->build();

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand(sprintf('.js-overview-builder-overview[data-overview-id=%s]', $plugin_id), $build));
    // Remove our overridden request object from the request stack.
    $this->requestStack->pop();
    return $response;
  }

  /**
   * Get the path from the request object.
   *
   * When AJAX is enabled, the current path is retrieved and posted in the
   * AJAX calls in Javascript. This method gets the submitted path from the
   * request object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return string|null
   *   The path when found, else NULL.
   */
  protected function getPathFromRequest(Request $request): ?string {
    $path = NULL;
    if (!empty($request->request->get('path'))) {
      $path = '/' . ltrim($request->request->get('path'), '/');
    }
    return $path;
  }

  /**
   * Override the current request and replace it with our own custom request.
   *
   * This method was added to make <current> URL's work when AJAX calls are
   * executed.
   *
   * @param \Symfony\Component\HttpFoundation\Request $initial_request
   *   The initial request object.
   */
  protected function overrideRequest(Request $initial_request): void {
    $request = Request::create((string) $this->getPathFromRequest($initial_request), 'POST', $initial_request->request->all(), $initial_request->cookies->all(), $initial_request->files->all(), $initial_request->server->all());
    $request->attributes->add($this->router->matchRequest($request));
    // Add all POST data, because AJAX is always a post and some items may
    // expect GET parameters.
    $request->query->replace($initial_request->request->all() + $initial_request->query->all());
    $this->requestStack->push($request);
  }

}
