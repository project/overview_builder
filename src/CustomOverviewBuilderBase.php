<?php

namespace Drupal\overview_builder;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\overview_builder\Exception\MissingEntityTypeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The custom overview builder base class.
 *
 * New custom overviews should extend this class.
 */
class CustomOverviewBuilderBase extends OverviewBuilderBase implements CacheableDependencyInterface {

  use StringTranslationTrait;

  /**
   * Nr of items shown per page.
   */
  protected const ITEMS_PER_PAGE = 10;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * The entity view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $entityViewBuilder;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * OverviewBuilderBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   *
   * @throws \Exception
   *   When no entity_type is configured in the annotation, throw an error.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, FormBuilderInterface $formBuilder, RequestStack $requestStack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    if ($this->getEntityType() === NULL) {
      // When no entity_type is configured in the annotation, throw an error.
      throw new MissingEntityTypeException(sprintf('No entity_type configured in %s.', get_class($this)));
    }

    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->entityStorage = $entityTypeManager->getStorage($this->getEntityType());
    $this->entityViewBuilder = $entityTypeManager->getViewBuilder($this->getEntityType());
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [
      '#theme' => 'overview_builder__' . $this->getPluginId(),
      '#wrapper_attributes' => [
        'class' => ['js-overview-builder-overview'],
        'data-overview-id' => $this->getPluginId(),
      ],
      '#filters' => $this->buildFilters(),
      '#items' => $this->buildItems(),
      '#pager' => $this->buildPager(),
      '#empty' => $this->getEmptyText(),
      '#cache' => [
        'tags' => $this->getCacheTags(),
        'contexts' => $this->getCacheContexts(),
        'max-age' => $this->getCacheMaxAge(),
      ],
    ];

    if ($this->getAjax() === TRUE) {
      $build['#attached']['library'][] = 'overview_builder/ajax_overview';
    }

    return $build;
  }

  /**
   * Build the filters. Returns a form if there are filters.
   *
   * @return array|null
   *   The filters form.
   */
  protected function buildFilters(): ?array {
    return NULL;
  }

  /**
   * Builds the items in the overview.
   *
   * @return array
   *   An array containing the items shown in the overview.
   */
  protected function buildItems(): array {
    $entities = $this->getListEntities();

    $items = [];
    if ($entities) {
      foreach ($entities as $entity) {
        $items[] = $this->entityViewBuilder->view($entity, $this->getViewMode());
      }
    }

    return $items;
  }

  /**
   * Build a list of entities shown in the overview.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The entities shown in the overview.
   */
  protected function getListEntities(): array {
    return $this->entityStorage->loadMultiple();
  }

  /**
   * Build the pager.
   *
   * @return array
   *   A renderable array containing the pager.
   */
  protected function buildPager(): array {
    return [
      '#type' => 'pager',
    ];
  }

  /**
   * Get the text shown when no results are found.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The text shown when no results are found.
   */
  protected function getEmptyText() {
    return $this->t('No results found.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [
      'url.query_args',
      'languages',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $cache_tag = $this->getEntityType() . '_list';

    if ($this->getBundle() !== NULL) {
      $cache_tag .= ':' . $this->getBundle();
    }

    return [
      $cache_tag,
    ];
  }

}
