<?php

namespace Drupal\overview_builder\Exception;

/**
 * Exception thrown if a custom overview builder plugin has no entity type.
 *
 * @see \Drupal\overview_builder\CustomOverviewBuilderBase
 */
class MissingEntityTypeException extends \Exception {

}
