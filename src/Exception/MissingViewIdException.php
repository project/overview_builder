<?php

namespace Drupal\overview_builder\Exception;

/**
 * Exception thrown if a views overview builder plugin has no view ID.
 *
 * @see \Drupal\overview_builder\ViewsOverviewBuilderBase
 */
class MissingViewIdException extends \Exception {

}
