<?php

namespace Drupal\overview_builder\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;

/**
 * Base class that contains helper methods for filter forms.
 *
 * Filter forms should extend this class.
 *
 * @package Drupal\overview_builder\Form
 */
abstract class CustomOverviewBuilderFiltersFormBase extends FormBase implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#action'] = Url::fromRoute('<current>')->toString();
    $form['#attributes']['class'][] = 'js-overview-builder-filters';
    $form['#method'] = 'GET';

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      // Prevent op from showing up in URL.
      '#name' => '',
      '#attributes' => ['class' => ['js-overview-builder-filters-submit']],
    ];

    $form['#cache']['contexts'] = [
      'languages',
      'url.query_args',
    ];

    $form['#pre_render'][] = [$this, 'removeHiddenFieldsfromGetParams'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Pre render function that removes hidden fields from the get params.
   *
   * @param array $form
   *   The form.
   *
   * @return array
   *   The altered form.
   */
  public function removeHiddenFieldsfromGetParams(array $form): array {
    unset($form['form_token'], $form['form_build_id'], $form['form_id']);
    return $form;
  }

  /**
   * Add a multiple options form element (select, checkboxes, radios).
   *
   * @param array $form
   *   The form array.
   * @param string $key
   *   The form element key.
   * @param string $title
   *   The form element title.
   * @param array $options
   *   The form element options.
   * @param string $type
   *   The form element type.
   * @param array $values
   *   Array containing extra form element settings.
   */
  protected function addMultiOptionsFormElement(array &$form, string $key, string $title, array $options, string $type = 'select', array $values = []) {
    $form[$key] = $values + [
      '#type' => $type,
      '#title' => $title,
      '#empty_option' => $this->t('- Choose -'),
      '#options' => $options,
      '#default_value' => $this->getRequest()->query->get($key, []),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['removeHiddenFieldsfromGetParams'];
  }

}
