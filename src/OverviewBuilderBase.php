<?php

namespace Drupal\overview_builder;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * The overview builder base class.
 *
 * This class contains some helper methods that almost every overview builder
 * needs. New overview builders should extend this class.
 */
abstract class OverviewBuilderBase extends PluginBase implements OverviewBuilderInterface, ContainerFactoryPluginInterface {

  /**
   * Get the entity type that is shown in the overview.
   *
   * @see \Drupal\overview_builder\CustomOverviewBuilderBase
   *
   * @return string|null
   *   The entity type.
   */
  protected function getEntityType(): ?string {
    return $this->pluginDefinition['entity_type'] ?? NULL;
  }

  /**
   * Get the bundle that is shown in the overview.
   *
   * @see \Drupal\overview_builder\CustomOverviewBuilderBase
   *
   * @return string|null
   *   The entity type.
   */
  protected function getBundle(): ?string {
    return $this->pluginDefinition['bundle'] ?? NULL;
  }

  /**
   * Get the configured view mode from the annotation.
   *
   * @see \Drupal\overview_builder\CustomOverviewBuilderBase
   *
   * @return string
   *   The view mode used in the overview.
   */
  protected function getViewMode(): string {
    return $this->pluginDefinition['view_mode'] ?? 'teaser';
  }

  /**
   * Ajax is enabled fr this overview.
   *
   * @see \Drupal\overview_builder\CustomOverviewBuilderBase
   *
   * @return bool
   *   boolean indicating that ajax is enabled or not.
   */
  protected function getAjax(): bool {
    return $this->pluginDefinition['ajax'] ?? FALSE;
  }

  /**
   * Get the configured view from the annotation.
   *
   * @see \Drupal\overview_builder\ViewsOverviewBuilderBase
   *
   * @return string|null
   *   The view ID or NULL when not configured.
   */
  protected function getViewId(): ?string {
    return $this->pluginDefinition['view_id'] ?? NULL;
  }

  /**
   * Get the configured display from the annotation.
   *
   * @see \Drupal\overview_builder\ViewsOverviewBuilderBase
   *
   * @return string|null
   *   The display ID or NULL when not configured.
   */
  protected function getDisplayId(): ?string {
    return $this->pluginDefinition['display_id'] ?? 'default';
  }

}
