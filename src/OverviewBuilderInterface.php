<?php

namespace Drupal\overview_builder;

/**
 * The Overview builder interface.
 *
 * New overview builder classes should implement this interface.
 *
 * @package Drupal\overview_builder
 */
interface OverviewBuilderInterface {

  /**
   * Builds the overview.
   *
   * @return array
   *   An array containing the overview.
   */
  public function build(): array;

}
