<?php

namespace Drupal\overview_builder\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of 'overview_builder_plugin_reference_build'.
 *
 * @FieldFormatter(
 *   id = "overview_builder_plugin_reference_build",
 *   label = @Translation("Plugin Reference overview builder"),
 *   field_types = {
 *     "plugin_reference"
 *   }
 * )
 */
class PluginReferenceOverviewBuilder extends FormatterBase {

  /**
   * The plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, PluginManagerInterface $plugin_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\field\Entity\FieldConfig $field_definition */
    $field_definition = $configuration['field_definition'];
    $target_type = $field_definition->getFieldStorageDefinition()
      ->getSetting('target_type');

    return new static(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.' . $target_type)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if ($this->pluginManager->hasDefinition($item->value)) {
        /** @var \Drupal\overview_builder\OverviewBuilderInterface $plugin */
        $plugin = $this->pluginManager->createInstance($item->value);
        $elements[$delta] = $plugin->build();
      }
    }

    return $elements;
  }

}
