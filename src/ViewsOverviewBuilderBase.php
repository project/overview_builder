<?php

namespace Drupal\overview_builder;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\overview_builder\Exception\MissingViewIdException;
use Drupal\views\Entity\View;
use Drupal\views\ViewExecutable;
use Drupal\views\ViewExecutableFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Views overview builder base class.
 *
 * New views overviews should extend this class.
 */
abstract class ViewsOverviewBuilderBase extends OverviewBuilderBase {

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $viewStorage;

  /**
   * The view executable factory.
   *
   * @var \Drupal\views\ViewExecutableFactory
   */
  protected $viewExecutableFactory;

  /**
   * ViewsOverviewBuilderBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\views\ViewExecutableFactory $viewExecutableFactory
   *   The view executable factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, ViewExecutableFactory $viewExecutableFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    if ($this->getViewId() === NULL) {
      // When no entity_type is configured in the annotation, throw an error.
      throw new MissingViewIdException(sprintf('No view configured in %s.', get_class($this)));
    }

    $this->viewStorage = $entityTypeManager->getStorage('view');
    $this->viewExecutableFactory = $viewExecutableFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('views.executable')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $renderable_view = $this->loadView($this->getViewId(), $this->getDisplayId());

    if ($renderable_view === NULL) {
      return [];
    }

    return $renderable_view;
  }

  /**
   * Returns the given view in the given display.
   *
   * @param string $view_id
   *   The view ID.
   * @param string $display_id
   *   The display ID.
   *
   * @return array|null
   *   return a renderable array or NULL when the view does not exist .
   */
  protected function loadView(string $view_id, string $display_id): ?array {
    $view_entity = $this->viewStorage->load($view_id);

    if (!$view_entity instanceof View) {
      return NULL;
    }

    $view = $this->viewExecutableFactory->get($view_entity);

    if (!$view instanceof ViewExecutable) {
      return NULL;
    }

    $view->setDisplay($display_id);
    $view->preExecute();
    $view->execute();
    return $view->buildRenderable($display_id);
  }

}
