<?php

namespace Drupal\overview_builder_test\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\overview_builder\Form\CustomOverviewBuilderFiltersFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form that adds the filters for the advanced custom news overview.
 *
 * @package Drupal\overview_builder_test\Form
 */
class AdvancedCustomNewsOverviewFiltersForm extends CustomOverviewBuilderFiltersFormBase {

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $taxonomyTermStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->taxonomyTermStorage = $entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advanced_custom_news_overview_filters_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->addMultiOptionsFormElement($form, 'news_category', $this->t('News category'), $this->getNewsCategoryOptions());

    $url = new Url('<current>');
    $url->toString();
    $form['reset_link'] = [
      '#type' => 'link',
      '#title' => 'Reset all filters',
      '#url' => new Url('<current>'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Build the news category options.
   *
   * @return array
   *   An array of news categories.
   */
  protected function getNewsCategoryOptions(): array {
    $options = [];

    $terms = $this->taxonomyTermStorage->loadByProperties(['vid' => 'news_category']);
    foreach ($terms as $term) {
      $options[$term->id()] = $term->label();
    }

    return $options;
  }

}
