<?php

namespace Drupal\overview_builder_test\Plugin\OverviewBuilder;

use Drupal\overview_builder\CustomOverviewBuilderBase;
use Drupal\overview_builder_test\Form\AdvancedCustomNewsOverviewFiltersForm;

/**
 * Simple custom node overview builder.
 *
 * @OverviewBuilder(
 *   id = "advanced_custom_news_overview",
 *   label = @Translation("Advanced Custom News overview"),
 *   entity_type = "node",
 *   bundle = "news",
 *   view_mode = "teaser",
 * )
 */
class AdvancedCustomNewsOverview extends CustomOverviewBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = parent::build();
    $build['#wrapper_attributes']['class'][] = 'my-custom-wrapper-overview-builder-class';
    $build['#attributes']['class'][] = 'my-custom-overview-builder-class';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildFilters(): ?array {
    return $this->formBuilder->getForm(AdvancedCustomNewsOverviewFiltersForm::class);
  }

  /**
   * {@inheritdoc}
   */
  public function getListEntities(): array {
    $query = $this->entityStorage->getQuery();

    $query->condition('type', 'news');
    $query->addTag('node_access');

    if ($this->currentRequest->query->has('news_category')) {
      $query->condition('field_news_category', $this->currentRequest->query->get('news_category'));
    }

    $query->pager(self::ITEMS_PER_PAGE);

    if ($this->currentRequest->query->has('sort') && $this->currentRequest->query->has('sort_direction')) {
      $query->sort(
        $this->currentRequest->query->get('sort'),
        $this->currentRequest->query->get('sort_direction')
      );
    }

    $results = $query->execute();
    if (empty($results)) {
      return [];
    }

    return $this->entityStorage->loadMultiple($results);
  }

}
