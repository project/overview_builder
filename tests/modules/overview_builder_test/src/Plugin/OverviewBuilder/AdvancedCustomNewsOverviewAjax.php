<?php

namespace Drupal\overview_builder_test\Plugin\OverviewBuilder;

/**
 * Advanced custom node overview builder with ajax.
 *
 * @OverviewBuilder(
 *   id = "advanced_custom_news_overview_ajax",
 *   label = @Translation("Advanced Custom News overview Ajax"),
 *   entity_type = "node",
 *   bundle = "news",
 *   view_mode = "teaser",
 *   ajax = true,
 * )
 */
class AdvancedCustomNewsOverviewAjax extends AdvancedCustomNewsOverview {

}
