<?php

namespace Drupal\overview_builder_test\Plugin\OverviewBuilder;

use Drupal\overview_builder\CustomOverviewBuilderBase;

/**
 * Custom overview missing entity type.
 *
 * Used in tests. This plugin will throw an error since it's missing an entity
 * type.
 *
 * @OverviewBuilder(
 *   id = "custom_overview_missing_entity_type",
 *   label = @Translation("Custom overview missing entity type")
 * )
 */
class CustomOverviewMissingEntityType extends CustomOverviewBuilderBase {

}
