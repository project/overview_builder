<?php

namespace Drupal\overview_builder_test\Plugin\OverviewBuilder;

use Drupal\overview_builder\ViewsOverviewBuilderBase;

/**
 * News overview builder.
 *
 * @OverviewBuilder(
 *   id = "news_views_overview",
 *   label = @Translation("News Views overview"),
 *   view_id = "news",
 *   display_id = "block_1",
 * )
 */
class NewsViewsOverview extends ViewsOverviewBuilderBase {

}
