<?php

namespace Drupal\overview_builder_test\Plugin\OverviewBuilder;

use Drupal\overview_builder\CustomOverviewBuilderBase;

/**
 * Simple custom node overview builder.
 *
 * @OverviewBuilder(
 *   id = "simple_custom_news_overview",
 *   label = @Translation("Simple Custom News overview"),
 *   entity_type = "node",
 *   view_mode = "teaser",
 * )
 */
class SimpleCustomNewsOverview extends CustomOverviewBuilderBase {

  /**
   * Nr of items shown per page.
   */
  protected const ITEMS_PER_PAGE = NULL;

  /**
   * {@inheritdoc}
   */
  public function getListEntities(): array {
    return $this->entityStorage->loadByProperties(['type' => 'news']);
  }

}
