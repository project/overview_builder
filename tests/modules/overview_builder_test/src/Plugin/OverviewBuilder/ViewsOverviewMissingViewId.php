<?php

namespace Drupal\overview_builder_test\Plugin\OverviewBuilder;

use Drupal\overview_builder\ViewsOverviewBuilderBase;

/**
 * Views overview missing view ID.
 *
 * Used in tests. This plugin will throw an error since it's missing a view ID.
 *
 * @OverviewBuilder(
 *   id = "views_overview_missing_view_id",
 *   label = @Translation("Views overview missing view ID")
 * )
 */
class ViewsOverviewMissingViewId extends ViewsOverviewBuilderBase {

}
