<?php

namespace Drupal\Tests\overview_builder\Functional;

use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Teset the overview builder in the overview paragraph.
 *
 * @group overview_builder
 */
class OverviewBuilderParagraphsTest extends BrowserTestBase {

  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['overview_builder_paragraphs_test'];

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * Nr of created news items.
   *
   * @var int
   */
  protected $createdNewsItems = 0;

  /**
   * An array of news category terms.
   *
   * @var array
   */
  protected $newsCategories = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($this->adminUser);

    $vocabulary = Vocabulary::load('news_category');
    $this->newsCategories = [
      $this->createTerm($vocabulary, ['name' => 'News category 1']),
      $this->createTerm($vocabulary, ['name' => 'News category 2']),
      $this->createTerm($vocabulary, ['name' => 'News category 3']),
      $this->createTerm($vocabulary, ['name' => 'News category 4']),
      $this->createTerm($vocabulary, ['name' => 'News category 5']),
    ];
  }

  /**
   * Test that overviews are picked up in the paragraphs overview widget.
   */
  public function testParagraphsWidget() {
    $this->drupalGet('/node/add/overview_page');

    $page = $this->getSession()->getPage();
    $page->findButton('field_paragraphs_add_more')->press();

    $assert_session = $this->assertSession();
    $assert_session->optionExists('field_paragraphs[0][subform][field_overview][0][value]', 'news_views_overview');
    $assert_session->optionExists('field_paragraphs[0][subform][field_overview][0][value]', 'simple_custom_news_overview');
    $assert_session->optionExists('field_paragraphs[0][subform][field_overview][0][value]', 'advanced_custom_news_overview');
  }

  /**
   * Test a views overview in the overview paragraph.
   */
  public function testParagraphsViewsOverview() {
    $this->drupalGet('/node/add/overview_page');

    $page = $this->getSession()->getPage();
    $page->findButton('field_paragraphs_add_more')->press();

    $title = $this->randomGenerator->string(8);
    $this->submitForm([
      'title[0][value]' => $title,
      'field_paragraphs[0][subform][field_overview][0][value]' => 'news_views_overview',
    ], 'Save');

    $this->assertSession()->pageTextContains('No results found.');

    $this->createNewsNodes(4);

    $node = $this->drupalGetNodeByTitle($title);
    $this->drupalGet($node->toUrl()->toString());

    $this->assertSession()->pageTextContains('News item 1');
    $this->assertSession()->pageTextContains('News item 2');
    $this->assertSession()->pageTextContains('News item 3');
    $this->assertSession()->pageTextContains('News item 4');
  }

  /**
   * Test a custom overview on the overview paragraph.
   */
  public function testParagraphsSimpleCustomOverview() {
    $this->drupalGet('/node/add/overview_page');

    $page = $this->getSession()->getPage();
    $page->findButton('field_paragraphs_add_more')->press();

    $title = $this->randomGenerator->string(8);
    $this->submitForm([
      'title[0][value]' => $title,
      'field_paragraphs[0][subform][field_overview][0][value]' => 'simple_custom_news_overview',
    ], 'Save');

    $this->assertSession()->pageTextContains('No results found.');

    $this->createNewsNodes(4);

    $node = $this->drupalGetNodeByTitle($title);
    $this->drupalGet($node->toUrl()->toString());

    $this->assertSession()->pageTextContains('News item 1');
    $this->assertSession()->pageTextContains('News item 2');
    $this->assertSession()->pageTextContains('News item 3');
    $this->assertSession()->pageTextContains('News item 4');
    $this->assertSession()->pageTextNotContains('News item 5');

    $this->createNewsNodes(1);

    $this->drupalGet($node->toUrl()->toString());
    $this->assertSession()->pageTextContains('News item 5');
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', 'node_list');
  }

  /**
   * Test a custom overview on the overview paragraph.
   */
  public function testParagraphsAdvancedCustomOverview() {
    $this->drupalGet('/node/add/overview_page');

    $page = $this->getSession()->getPage();
    $page->findButton('field_paragraphs_add_more')->press();

    $title = $this->randomGenerator->string(8);
    $this->submitForm([
      'title[0][value]' => $title,
      'field_paragraphs[0][subform][field_overview][0][value]' => 'advanced_custom_news_overview',
    ], 'Save');

    $this->assertSession()->pageTextContains('No results found.');

    $this->createNewsNodes(35);

    $node = $this->drupalGetNodeByTitle($title);
    $this->drupalGet($node->toUrl()->toString());
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', 'node_list:news');
    $this->assertSession()->elementExists('css', '.my-custom-wrapper-overview-builder-class');
    $this->assertSession()->elementExists('css', '.my-custom-overview-builder-class');
    $this->assertSession()->responseNotContains('overview_builder/assets/js/overview_builder.ajax_overview.js');

    // Check if filtering works.
    $this->drupalGet($node->toUrl()->toString(), ['query' => ['news_category' => $this->newsCategories[0]->id()]]);
    $this->assertSession()->selectExists('news_category');

    $selected_option = $this->assertSession()->optionExists('news_category', $this->newsCategories[0]->id());
    $this->assertEquals('selected', $selected_option->getAttribute('selected'));

    $not_selected_options = $this->assertSession()->optionExists('news_category', $this->newsCategories[1]->id());
    $this->assertEmpty($not_selected_options->getAttribute('selected'));

    // Check if sorting works.
    $this->drupalGet($node->toUrl()->toString(), [
      'query' => [
        'sort' => 'title',
        'sort_direction' => 'desc',
      ],
    ]);
    $this->assertSession()->pageTextContains('News item 9');
    $this->assertSession()->pageTextContains('News item 8');
    $this->assertSession()->pageTextNotContains('News item 1');
    $this->assertSession()->pageTextNotContains('News item 2');

    // Check if pagination works.
    $this->drupalGet($node->toUrl()->toString());
    $this->assertSession()->elementExists('css', '.paragraph--type--overview .pager');
    $this->assertSession()->elementTextContains('css', '.paragraph--type--overview .pager .pager__item.is-active', 1);
    $this->assertSession()->pageTextContains('News item 1');
    $this->assertSession()->pageTextContains('News item 2');
    $this->assertSession()->pageTextNotContains('News item 31');
    $this->assertSession()->pageTextNotContains('News item 35');

    $this->drupalGet($node->toUrl()->toString(), ['query' => ['page' => 3]]);

    $this->assertSession()->elementExists('css', '.paragraph--type--overview .pager');
    $this->assertSession()->elementTextContains('css', '.paragraph--type--overview .pager .pager__item.is-active', 4);
    $this->assertSession()->pageTextNotContains('News item 1');
    $this->assertSession()->pageTextNotContains('News item 2');
    $this->assertSession()->pageTextContains('News item 31');
    $this->assertSession()->pageTextContains('News item 35');
  }

  /**
   * Test access checks for the overview builder ajax controller.
   */
  public function testAjaxAccessChecks() {
    $this->drupalGet('/node/add/overview_page');
    $title = $this->randomString();
    $this->submitForm([
      'title[0][value]' => $title,
    ], 'Save');
    $node = $this->drupalGetNodeByTitle($title);
    $current_url = 'node/' . $node->id();

    // An unexisting overview will result in a 404 error.
    $url = Url::fromRoute('overview_builder.ajax.overview', ['plugin_id' => 'unexisting_overview']);
    $response = $this->request('POST', $url);
    $this->assertEquals(404, $response->getStatusCode());

    $url = Url::fromRoute('overview_builder.ajax.overview', ['plugin_id' => 'unexisting_overview']);
    $response = $this->request('POST', $url, ['form_params' => ['path' => $current_url]]);
    $this->assertEquals(404, $response->getStatusCode());

    // An overview without AJAX enabled, will result in a 404 error.
    $url = Url::fromRoute('overview_builder.ajax.overview', ['plugin_id' => 'advanced_custom_news_overview']);
    $response = $this->request('POST', $url);
    $this->assertEquals(404, $response->getStatusCode());

    $url = Url::fromRoute('overview_builder.ajax.overview', ['plugin_id' => 'advanced_custom_news_overview']);
    $response = $this->request('POST', $url, ['form_params' => ['path' => $current_url]]);
    $this->assertEquals(404, $response->getStatusCode());

    // An overview with AJAX enabled will only return a valid request when a
    // valid path parameter is added.
    $url = Url::fromRoute('overview_builder.ajax.overview', ['plugin_id' => 'advanced_custom_news_overview_ajax']);
    $response = $this->request('POST', $url);
    $this->assertEquals(404, $response->getStatusCode());

    $url = Url::fromRoute('overview_builder.ajax.overview', ['plugin_id' => 'advanced_custom_news_overview_ajax']);
    $response = $this->request('POST', $url, ['form_params' => ['path' => $this->randomMachineName()]]);
    $this->assertEquals(404, $response->getStatusCode());

    $url = Url::fromRoute('overview_builder.ajax.overview', ['plugin_id' => 'advanced_custom_news_overview_ajax']);
    $response = $this->request('POST', $url, ['form_params' => ['path' => $current_url]]);
    $this->assertEquals(200, $response->getStatusCode());

  }

  /**
   * Helper method that makes some dummy news items.
   *
   * @param int $nr_of_news_items
   *   The nr of news items to create.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createNewsNodes($nr_of_news_items) {
    for ($i = 1; $i <= $nr_of_news_items; $i++) {
      $this->createdNewsItems++;
      $this->drupalCreateNode([
        'type' => 'news',
        'title' => sprintf('News item %s', $this->createdNewsItems),
        'field_news_category' => ['target_id' => array_rand($this->newsCategories)],
      ]);
    }
  }

  /**
   * Performs a HTTP request.
   *
   * @param string $method
   *   HTTP method.
   * @param \Drupal\Core\Url $url
   *   URL to request.
   * @param array $request_options
   *   Request options to apply.
   *
   * @see \GuzzleHttp\ClientInterface::request()
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  protected function request($method, Url $url, array $request_options = []): ResponseInterface {
    $request_options[RequestOptions::HTTP_ERRORS] = FALSE;
    $request_options[RequestOptions::ALLOW_REDIRECTS] = FALSE;
    $client = $this->getHttpClient();
    return $client->request($method, $url->setAbsolute(TRUE)->toString(), $request_options);
  }

}
