<?php

namespace Drupal\Tests\overview_builder\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Test the custom overview builder with AJAX enabled.
 *
 * @group overview_builder
 */
class OverviewBuilderAjaxTest extends WebDriverTestBase {

  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['overview_builder_paragraphs_test'];

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * Nr of created news items.
   *
   * @var int
   */
  protected $createdNewsItems = 0;

  /**
   * An array of news category terms.
   *
   * @var array
   */
  protected $newsCategories = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($this->adminUser);

    $vocabulary = Vocabulary::load('news_category');
    $this->newsCategories = [
      $this->createTerm($vocabulary, ['name' => 'News category 1']),
      $this->createTerm($vocabulary, ['name' => 'News category 2']),
      $this->createTerm($vocabulary, ['name' => 'News category 3']),
      $this->createTerm($vocabulary, ['name' => 'News category 4']),
      $this->createTerm($vocabulary, ['name' => 'News category 5']),
    ];
  }

  /**
   * Test a custom overview on the overview paragraph.
   */
  public function testParagraphsAdvancedCustomOverview() {
    $this->drupalGet('/node/add/overview_page');

    $page = $this->getSession()->getPage();
    $page->findButton('field_paragraphs_add_more')->press();
    $this->assertSession()->waitForElementVisible('css', '[name="field_paragraphs[0][subform][field_overview][0][value]"]');

    $title = $this->randomGenerator->string(8);
    $this->submitForm([
      'title[0][value]' => $title,
      'field_paragraphs[0][subform][field_overview][0][value]' => 'advanced_custom_news_overview_ajax',
    ], 'Save');

    $this->assertSession()->pageTextContains('No results found.');
    $this->assertSession()->responseContains('overview_builder/assets/js/overview_builder.ajax_overview.js');

    $this->createNewsNodes(35);
    $this->drupalCreateNode([
      'type' => 'news',
      'title' => 'News item 36',
      'field_news_category' => ['target_id' => $this->newsCategories[2]->id()],
    ]);

    $node = $this->drupalGetNodeByTitle($title);
    $this->drupalGet('node/' . $node->id());

    // Check if filtering works.
    $submit_button = $this->assertSession()->buttonExists('Search');

    $news_category_filter = $this->assertSession()->selectExists('news_category');
    $news_category_filter->selectOption($this->newsCategories[0]->id());
    $submit_button->click();
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->responseNotContains("News item 36");

    $selected_option = $this->assertSession()->optionExists('news_category', $this->newsCategories[0]->id());
    $this->assertEquals('selected', $selected_option->getAttribute('selected'));

    $not_selected_options = $this->assertSession()->optionExists('news_category', $this->newsCategories[1]->id());
    $this->assertEmpty($not_selected_options->getAttribute('selected'));

    // Check if url is updated.
    $this->assertUpdatedUrl('node/' . $node->id(), [
      'news_category' => $this->newsCategories[0]->id(),
    ]);

    // Check that <current> links work after an ajax call.
    $reset_link = $page->findLink('Reset all filters');
    $this->assertEquals($node->toUrl()->toString(), $reset_link->getAttribute('href'));

    // Check that the action URL of the form is updated after an AJAX call.
    $filters_form = $page->find('css', '.js-overview-builder-filters');
    $this->assertEquals($node->toUrl()->toString(), $filters_form->getAttribute('action'));

    // Check if multiple ajax calls work.
    $news_category_filter->selectOption($this->newsCategories[1]->id());
    $submit_button->click();
    $this->assertSession()->assertWaitOnAjaxRequest();

    $selected_option = $this->assertSession()->optionExists('news_category', $this->newsCategories[1]->id());
    $this->assertEquals('selected', $selected_option->getAttribute('selected'));

    $not_selected_options = $this->assertSession()->optionExists('news_category', $this->newsCategories[0]->id());
    $this->assertEmpty($not_selected_options->getAttribute('selected'));

    // Check if url is updated.
    $this->assertUpdatedUrl('node/' . $node->id(), [
      'news_category' => $this->newsCategories[1]->id(),
    ]);

    // Check if pagination works.
    $this->drupalGet('node/' . $node->id());
    $pager = $this->assertSession()->elementExists('css', '.js-pager__items');
    $pager->clickLink('Next');

    $this->assertSession()->assertWaitOnAjaxRequest();

    // Check if url is updated.
    $this->assertUpdatedUrl('node/' . $node->id(), ['page' => 1]);

    $pager->clickLink('Last');
    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->assertSession()->pageTextNotContains('News item 1');
    $this->assertSession()->pageTextNotContains('News item 2');
    $this->assertSession()->pageTextContains('News item 31');
    $this->assertSession()->pageTextContains('News item 35');

    // Check if url is updated.
    $this->assertUpdatedUrl('node/' . $node->id(), ['page' => 3]);

    // Check if filtering and pagination works together.
    // Create 40 news items of the same category so pagination will be
    // activated.
    $this->createNewsNodes(40, ['field_news_category' => ['target_id' => $this->newsCategories[0]->id()]]);

    $submit_button = $this->assertSession()->buttonExists('Search');

    $news_category_filter = $this->assertSession()->selectExists('news_category');
    $news_category_filter->selectOption($this->newsCategories[0]->id());
    $submit_button->click();
    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->assertUpdatedUrl('node/' . $node->id(), [
      'news_category' => $this->newsCategories[0]->id(),
    ]);

    $pager = $this->assertSession()->elementExists('css', '.js-pager__items');
    $pager->clickLink('Next');
    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->assertUpdatedUrl('node/' . $node->id(), [
      'news_category' => $this->newsCategories[0]->id(),
      'page' => 1,
    ]);
  }

  /**
   * Helper method that makes some dummy news items.
   *
   * @param int $nr_of_news_items
   *   The nr of news items to create.
   * @param array $values
   *   Add default values to the created news nodes.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createNewsNodes($nr_of_news_items, array $values = []) {
    for ($i = 1; $i <= $nr_of_news_items; $i++) {
      $this->createdNewsItems++;
      $this->drupalCreateNode($values + [
        'type' => 'news',
        'title' => sprintf('News item %s', $this->createdNewsItems),
        'field_news_category' => ['target_id' => array_rand($this->newsCategories)],
      ]);
    }
  }

  /**
   * Check if the URL is updated and matches the given path and get parameters.
   *
   * @param string $path
   *   The expected path.
   * @param array $expected_get_params
   *   The expected GET parameters.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function assertUpdatedUrl(string $path, array $expected_get_params = []): void {
    $this->assertSession()->addressEquals($path);
    $get_params = [];

    $query_url = parse_url($this->getSession()->getCurrentUrl(), PHP_URL_QUERY);
    $this->assertEquals(http_build_query($expected_get_params), $query_url);
    parse_str($query_url, $get_params);
    $this->assertEquals($expected_get_params, $get_params);
  }

}
