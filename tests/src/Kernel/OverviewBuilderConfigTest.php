<?php

namespace Drupal\Tests\overview_builder\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * Test the initial overview builder config.
 *
 * @group overview_builder
 */
class OverviewBuilderConfigTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'field',
    'paragraphs',
    'pluginreference',
    'overview_builder',
  ];

  /**
   * The entity form display storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityFormDisplay;

  /**
   * The entity view display storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityViewDisplay;

  /**
   * The field config storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fieldConfigStorage;

  /**
   * The paragraphs type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paragraphsTypeStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $this->entityFormDisplay = $entity_type_manager->getStorage('entity_form_display');
    $this->entityViewDisplay = $entity_type_manager->getStorage('entity_view_display');
    $this->fieldConfigStorage = $entity_type_manager->getStorage('field_config');
    $this->paragraphsTypeStorage = $entity_type_manager->getStorage('paragraphs_type');

    $this->installConfig(['pluginreference', 'overview_builder']);
  }

  /**
   * Test the installed config.
   */
  public function testInstalledConfig() {
    // Check that the overview paragraph is added when the module is installed.
    $overview_paragraph_type = $this->paragraphsTypeStorage->load('overview');
    $this->assertInstanceOf(ParagraphsType::class, $overview_paragraph_type);

    // Check that the overview field was added.
    $field_config = $this->fieldConfigStorage->load('paragraph.overview.field_overview');
    $this->assertInstanceOf(FieldConfig::class, $field_config);
    $this->assertEquals('plugin_reference', $field_config->getType());

    // Check the form widget for the overview field.
    $paragraph_overview_entity_form_display = $this->entityFormDisplay->load('paragraph.overview.default');
    $component = $paragraph_overview_entity_form_display->getComponent('field_overview');
    $this->assertArrayHasKey('type', $component);
    $this->assertEquals('plugin_reference_select', $component['type']);

    // Check the view widget for the overview field.
    $paragraph_overview_entity_view_display = $this->entityViewDisplay->load('paragraph.overview.default');
    $component = $paragraph_overview_entity_view_display->getComponent('field_overview');
    $this->assertArrayHasKey('type', $component);
    $this->assertEquals('overview_builder_plugin_reference_build', $component['type']);
  }

}
