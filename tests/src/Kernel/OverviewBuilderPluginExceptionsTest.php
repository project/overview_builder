<?php

namespace Drupal\Tests\overview_builder\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\overview_builder\Exception\MissingEntityTypeException;
use Drupal\overview_builder\Exception\MissingViewIdException;

/**
 * Test the exceptions thrown by wrongfully configured overview builder plugins.
 *
 * @group overview_builder
 */
class OverviewBuilderPluginExceptionsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'user',
    'system',
    'taxonomy',
    'text',
    'views',
    'pluginreference',
    'overview_builder',
    'overview_builder_test',
  ];

  /**
   * The overview builder plugin manager.
   *
   * @var Drupal\overview_builder\OverviewBuilderManager
   */
  protected $overviewBuilderManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('taxonomy_term');
    $this->installConfig([
      'field',
      'pluginreference',
      'overview_builder',
      'overview_builder_test',
    ]);
    $this->overviewBuilderManager = $this->container->get('plugin.manager.overview_builder');
  }

  /**
   * Test a views overview builder plugin without a view ID.
   */
  public function testMissingViewId() {
    $this->expectException(MissingViewIdException::class);
    $this->expectExceptionMessage('No view configured in Drupal\overview_builder_test\Plugin\OverviewBuilder\ViewsOverviewMissingViewId.');
    $this->overviewBuilderManager->createInstance('views_overview_missing_view_id');
  }

  /**
   * Test a custom overview builder plugin without an entity type.
   */
  public function testMissingEntityType() {
    $this->expectException(MissingEntityTypeException::class);
    $this->expectExceptionMessage('No entity_type configured in Drupal\overview_builder_test\Plugin\OverviewBuilder\CustomOverviewMissingEntityType.');
    $this->overviewBuilderManager->createInstance('custom_overview_missing_entity_type');
  }

}
